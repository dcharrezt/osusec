
## Hash browns


### Summary

This challenges shows that although sha256 in one-way encryption, some databases store known hashes.

### SHA256

Is a cryptographic hash function, given an input it returns its hash.

```bash
hi -> 8f434346648f6b96df89dda901c5176b10a6d83961dd3c1ac88b59b2dc327aa4
hello -> 2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
```

One could try guessing inputs until you get the desired hash function but that does not guaranted the right input value. multiple inputs can generate the same hash (collision).

### Explanation

Inspecting the website the submit button reveals the on_click function, that can be seen in the script html section. The function is just comparing inputs with a hash thus looking the hash up on an online database reveals an input that generates the same hash. Then using that input we access the page where the flag is revealed.
